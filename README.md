# Kimera-VIO for Monado

This is a fork of [Kimera-VIO](https://github.com/MIT-SPARK/Kimera-VIO/) with
some modifications so that it can be used from Monado for SLAM tracking. Many
thanks to the Kimera-VIO authors.

## Index

- [Kimera-VIO for Monado](#kimera-vio-for-monado)
  - [Index](#index)
  - [Installation](#installation)
    - [Build and Install Directories](#build-and-install-directories)
    - [Dependencies](#dependencies)
      - [Dependencies in APT](#dependencies-in-apt)
      - [GTSAM](#gtsam)
      - [OpenCV 3.3.1](#opencv-331)
      - [OpenGV](#opengv)
      - [DBoW2](#dbow2)
      - [Kimera-RPGO](#kimera-rpgo)
    - [Kimera-VIO](#kimera-vio)
    - [Monado Specifics](#monado-specifics)
  - [Using a RealSense Camera](#using-a-realsense-camera)
    - [Overview of the Setup (D455)](#overview-of-the-setup-d455)
      - [SLAM-Tracked RealSense Driver](#slam-tracked-realsense-driver)
      - [RealSense-Tracked Qwerty Driver](#realsense-tracked-qwerty-driver)
    - [Non-D455 RealSense Devices](#non-d455-realsense-devices)
      - [Configuring the RealSense Pipeline](#configuring-the-realsense-pipeline)
      - [Configuring Kimera-VIO](#configuring-kimera-vio)
    - [Notes on Kimera-VIO Usage](#notes-on-kimera-vio-usage)

## Installation

This was tested on Ubuntu 20.04, 22.04, 18.04, and 21.04 (in order of
thoroughness), be sure to open an issue if the steps don't work for you.

### Build and Install Directories

To not clutter your system directories, let's set two environment variables,
`$kviodeps` and `$kvioinstall` that point to existing empty build and install
directories respectively. These directories will contain everything (besides apt
dependencies) produced in this guide.

```bash
# Change the paths accordingly
export kvioinstall=/home/mateo/Documents/apps/kvioinstall
export kviodeps=/home/mateo/Documents/apps/kviodeps
```

Let's extend our system paths with those.

```bash
export PATH=$kvioinstall/bin:$PATH
export PKG_CONFIG_PATH=$kvioinstall/lib/pkgconfig:$PKG_CONFIG_PATH # for compile time pkg-config
export LD_LIBRARY_PATH=$kvioinstall/lib/:$LD_LIBRARY_PATH # for runtime ld
export LIBRARY_PATH=$kvioinstall/lib/:$LIBRARY_PATH # for compile time gcc
```

### Dependencies

*Note: if any dependency is missing, please take a look at the compilation guide
from the original
[Kimera-VIO](https://github.com/MIT-SPARK/Kimera-VIO/blob/master/docs/kimera_vio_install.md)
repo, although those instructions have been a bit problematic in my experience.*

#### Dependencies in APT

```bash
sudo apt update
sudo apt install -y --no-install-recommends \
  apt-utils cmake libboost-all-dev build-essential unzip pkg-config unzip \
  libjpeg-dev libpng-dev libtiff-dev libvtk6-dev libgtk-3-dev libparmetis-dev \
  unzip libatlas-base-dev gfortran libtbb-dev libgflags-dev libgoogle-glog-dev
```

#### GTSAM

```bash
cd $kviodeps
git clone https://github.com/borglab/gtsam.git
cd gtsam && mkdir build && cd build
git checkout 37ea955d
cmake .. -DCMAKE_INSTALL_PREFIX=$kvioinstall -DCMAKE_BUILD_TYPE=Release -DGTSAM_USE_SYSTEM_EIGEN=OFF -DGTSAM_POSE3_EXPMAP=ON -DGTSAM_ROT3_EXPMAP=ON -DGTSAM_TANGENT_PREINTEGRATION=OFF -DGTSAM_BUILD_WITH_MARCH_NATIVE=ON
cmake --build . --target check -- -j $(nproc) # Should pass 100%
cmake --build . --target install -- -j $(nproc)
```
If compilation fails because of boost headers, it is because of a missing header
in the boost version shipped in Ubuntu. You will need to edit your installed
boost headers like
[this](https://github.com/boostorg/serialization/commit/f72b9fc8d953a5dd39615535b5c6bab5b8be42fe).

#### OpenCV 3.3.1

```bash
cd $kviodeps
wget https://github.com/opencv/opencv/archive/3.3.1.zip
unzip 3.3.1.zip
cd opencv-3.3.1/ && mkdir build && cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$kvioinstall -DWITH_VTK=On -DWITH_TBB=On
cmake --build . --target install -- -j $(nproc)
```

If compilation fails with something related to some macros like
`AV_CODEC_FLAG_GLOBAL_HEADER`, add these defines at the start of the file
`opencv-3.3.1/modules/videoio/src/cap_ffmpeg_impl.hpp`:

```c++
#define CODEC_FLAG_GLOBAL_HEADER AV_CODEC_FLAG_GLOBAL_HEADER
#define AVFMT_RAWPICTURE 0x0020
```

If compilation fails with a `const` error, change `cv2.cpp:856` with this

```bash
sed -i 's/  char\* str/  const char\* str/' $kviodeps/opencv-3.3.1/modules/python/src2/cv2.cpp
```

#### OpenGV

```bash
cd $kviodeps
git clone https://github.com/laurentkneip/opengv.git
cd opengv && mkdir build && cd build
git checkout 91f4b19c
cmake .. -DCMAKE_INSTALL_PREFIX=$kvioinstall -DEIGEN_INCLUDE_DIR=$kviodeps/gtsam/gtsam/3rdparty/Eigen -DEIGEN_INCLUDE_DIRS=$kviodeps/gtsam/gtsam/3rdparty/Eigen
cmake --build . --target install -- -j $(nproc)
```

#### DBoW2

```bash
cd $kviodeps
git clone https://github.com/dorian3d/DBoW2.git
cd DBoW2/ && mkdir build && cd build
git checkout 3924753d
cmake .. -DCMAKE_INSTALL_PREFIX=$kvioinstall
cmake --build . --target install -- -j $(nproc)
```

#### Kimera-RPGO

```bash
cd $kviodeps
git clone https://github.com/MIT-SPARK/Kimera-RPGO.git
cd Kimera-RPGO && mkdir build && cd build
git checkout 8fdb14ea
cmake .. -DCMAKE_INSTALL_PREFIX=$kvioinstall -DEIGEN_INCLUDE_DIR=$kviodeps/gtsam/gtsam/3rdparty/Eigen -DEIGEN_INCLUDE_DIRS=$kviodeps/gtsam/gtsam/3rdparty/Eigen
cmake --build . --target install -- -j $(nproc)
```

### Kimera-VIO

```bash
cd $kviodeps
git clone https://gitlab.freedesktop.org/mateosss/Kimera-VIO
cd Kimera-VIO && mkdir build && cd build
git checkout xrtslam
sed -i "s#/home/mateo/Documents/apps/kviodeps/#$kviodeps/#" ../params/Euroc/flags/Monado.flags
cmake .. -DCMAKE_INSTALL_PREFIX=$kvioinstall -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
cmake --build . --target install -- -j6 # You can up -j6 if you have more than 16GB of RAM
```

If having problems with an invalid use of `auto`, add -DCMAKE_CXX_STANDARD=14

### Monado Specifics

You'll need to compile Monado with the same Eigen and `-march=native` flags, i.e.: add
`-DEIGEN3_INCLUDE_DIR=$kviodeps/gtsam/gtsam/3rdparty/Eigen -DCMAKE_C_FLAGS="-march=native"
-DCMAKE_CXX_FLAGS="-march=native"` when building otherwise you will get difficult-to-debug
segfaults.
Then run an OpenXR app like `hello_xr` with the following environment variables:

```bash
export EUROC_PATH=/path/to/euroc/V1_01_easy/ # Set euroc dataset path. You can get a dataset from http://robotics.ethz.ch/~asl-datasets/ijrr_euroc_mav_dataset/vicon_room1/V1_01_easy/V1_01_easy.zip
export EUROC_LOG=debug
export EUROC_HMD=false # if false a fake controller will be tracked, else a fake HMD
export SLAM_LOG=debug
export SLAM_CONFIG=$kviodeps/Kimera-VIO/params/Euroc/flags/Monado.flags # Point to kimera config file
export OXR_DEBUG_GUI=1 # We will need the debug ui to start streaming the dataset
```
Finally, press start in the euroc player debug ui and you should see a
controller being tracked with kimera from the euroc dataset.

## Using a RealSense Camera

### Overview of the Setup (D455)

Let's first assume you have a RealSense D455, which is the one that works with
the defaults. Even if you have another RealSense device follow this section, you
might at least get something working, although not at its best.

#### SLAM-Tracked RealSense Driver

Set these environment variables

```bash
export RS_HDEV_LOG=debug # To enable debug logging for our device
export RS_SOURCE_INDEX=0 # Indicate that we want to use the first RealSense device connected as data source
export RS_TRACKING=2 # To instruct the realsense prober to create a Host-SLAM tracked device, (see other [possible values](https://gitlab.freedesktop.org/mateosss/monado/-/blob/9e1b7e2203ef49abb939cc8fc92afa16fcc9cb3a/src/xrt/drivers/realsense/rs_prober.c#L33-39))
export SLAM_CONFIG=$kviodeps/Kimera-VIO/params/D455-stereo/flags/Monado.flags # To tell Kimera to use a configuration for the D455 with both cameras.
```

#### RealSense-Tracked Qwerty Driver

You now have a RealSense device that you can use to track another device, for
example, let's track a Qwerty HMD:

Set these environment variables to enable the qwerty driver.

```bash
export QWERTY_ENABLE=true
export QWERTY_COMBINE=true
```

And then modify your tracking overrides in your monado configuration file
(`~/.config/monado/config_v0.json`) by updating the json object with:
```js
{
  "tracking": {
    "tracking_overrides": [
      {
        "target_device_serial": "Qwerty HMD",
        "tracker_device_serial": "Intel RealSense Host-SLAM",
        "type": "direct",
        "offset": {
          "orientation": { "x": 0, "y": 0, "z": 0, "w": 1 },
          "position": { "x": 0, "y": 0, "z": 0 }
        },
        "xrt_input_name": "XRT_INPUT_GENERIC_TRACKER_POSE"
      }
    ],
  }
}
```

And that's it! You can now start a OpenXR application with Monado and get your
view tracked with your D455 camera.

### Non-D455 RealSense Devices

While I was unable to test other devices because I don't have access to them, it
should be possible to make them work by:

#### Configuring the RealSense Pipeline

[These
fields](https://gitlab.freedesktop.org/mateosss/monado/-/blob/9e1b7e2203ef49abb939cc8fc92afa16fcc9cb3a/src/xrt/drivers/realsense/rs_hdev.c#L118-129)
determine your RealSense streaming configuration, and
[these](https://gitlab.freedesktop.org/mateosss/monado/-/blob/9e1b7e2203ef49abb939cc8fc92afa16fcc9cb3a/src/xrt/drivers/realsense/rs_hdev.c#L40-50)
are their current defaults that work on a D455. You can change those fields by
setting any of them in your `config_v0.json` inside a `config_realsense_hdev`
field. Also note that as we set `RS_HDEV_LOG=debug`, you should see the values
they are currently taking at the start of Monado.

For example, let's say you have a realsense device which has two fisheye cameras
that support streaming 640x360 at 30fps, then a configuration like this should
work:
```js
"realsense_config_hdev": {
  "stereo": true,
  "video_format": 9, // 9 gets casted to RS2_FORMAT_Y8 (see https://git.io/Jzkfw), grayscale
  "video_width": 640, // I am assuming the T265 supports 640x360 streams at 30fps
  "video_height": 360,
  "video_fps": 30,
  "gyro_fps": 0, // 0 indicates any
  "accel_fps": 0,
  "stream_type": 4, // 4 gets casted to RS2_STREAM_FISHEYE (see https://git.io/Jzkvq)
  "stream1_index": -1, // If there were more than one possible stream with these properties select them, -1 is for auto
  "stream2_index": -1,
}
```
The particular values you could set here are very dependant on your camera. I
recommend seeing the values that get output by running the [rs-sensor-control
example](https://dev.intelrealsense.com/docs/rs-sensor-control) from the
RealSense API.

#### Configuring Kimera-VIO

As you might've noticed, we set `SLAM_CONFIG` to
`$kviodeps/Kimera-VIO/params/D455-stereo/flags/Monado.flags` which is
[this](params/D455-stereo/flags/Monado.flags) config file that I added for the
D455 stereo configuration.

For the tracking to be as good as possible you should set the
intrinsics/extrinsics of the device in the following Kimera config files.

I recommend you duplicating the `D455-stereo/` into a new `my-device` directory
and then
- Replace `D455-stereo` string in `Monado.flags` for `my-device` as appropriate.
- Configure intrinsics/extrinsics of `RightCameraParams.yaml`,
  `LeftCameraParams.yaml` and `ImuParams.yaml`. They can be obtained from the
  same
  [rs-sensor-control](https://dev.intelrealsense.com/docs/rs-sensor-control)
  utility.
- Set the `SLAM_CONFIG` property to point to the new `Monado.flags` file.

### Notes on Kimera-VIO Usage

- For now, this fork is an initial approximation into using Kimera-VIO with
  Monado. The tracking is bad, [here](https://youtu.be/ajuqQ7E1MFw) is an
  example of how it should look. For a better tracking alternative see this
  [ORB_SLAM3](https://gitlab.freedesktop.org/mateosss/ORB_SLAM3) fork, with the
  downside being that ORB_SLAM3 is GPL.
- Kimera should work with both Stereo-IMU and Monocular-IMU pipelines, although
  it was initially created to support only Stereo-IMU.
