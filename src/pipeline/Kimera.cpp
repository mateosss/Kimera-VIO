// Copyright 2021, Collabora, Ltd.

/**
 * @file   Kimera.cpp
 * @brief  Expose Kimera as a library for external usage
 * @author Mateo de Mayo <mateo.demayo@collabora.com>
 */

#include "kimera-vio/pipeline/Kimera.h"

#include <future>
#include <string>

#include "kimera-vio/common/VioNavState.h"
#include "kimera-vio/dataprovider/EurocDataProvider.h"
#include "kimera-vio/imu-frontend/ImuFrontend-definitions.h"
#include "kimera-vio/pipeline/MonoImuPipeline.h"
#include "kimera-vio/pipeline/Pipeline.h"
#include "kimera-vio/pipeline/StereoImuPipeline.h"
#include "kimera-vio/third_party/monado/slam_tracker.hpp"

DEFINE_string(
    wrapper_params_folder_path,
    "../params/Euroc",
    "Path to the folder containing the yaml files with the VIO parameters.");

using namespace VIO;

namespace VIO {

using std::make_shared;
using std::shared_ptr;
using std::static_pointer_cast;
using std::string;
using std::vector;

static vector<string> timing_titles{
    "frame_ts",
    "tracker_received",
    "frames_received",
    "pose_produced",
    "pose_pushed",
    "monado_dequeued",
};

Kimera::Kimera(std::string flagfile) : tk_{false, &timing_titles} {
  google::ReadFromFlagsFile(flagfile, "Kimera", true);
  google::InitGoogleLogging("Kimera");
  LOG(INFO) << "Wrapper params directory: " << FLAGS_wrapper_params_folder_path;

  vio_params_ = std::shared_ptr<VioParams>(
      new VioParams(FLAGS_wrapper_params_folder_path));
  VioParams& vio_params = *vio_params_;

  is_parallel_ = vio_params.parallel_run_;
  is_stereo_ = vio_params.frontend_type_ == FrontendType::kStereoImu;

  if (is_stereo_) {
    vio_pipeline_ = make_unique<StereoImuPipeline>(vio_params);
  } else {
    vio_pipeline_ = make_unique<MonoImuPipeline>(vio_params);
  }

  vio_pipeline_->registerBackendOutputCallback(
      [this](auto o) { this->receivePoseFromBackend(o); });

  vio_pipeline_->registerShutdownCallback(
      [this]() { this->is_running_ = false; });
}

void Kimera::start() {
  is_running_ = true;
  awaiting_frame_ = 0;

  if (is_parallel_) {
    auto pipe = std::async(std::launch::async, &Pipeline::spin, vio_pipeline_);
    vio_pipeline_->spinViz();
    pipe.get();
  }
}

void Kimera::stop() { vio_pipeline_->shutdown(); }

bool Kimera::isRunning() { return is_running_; }

void Kimera::pushImuSample(std::int64_t t,
                           double ax,
                           double ay,
                           double az,
                           double wx,
                           double wy,
                           double wz) {
  if (!is_running_) return;
  Vector6 accgyr;
  accgyr << ax, ay, az, wx, wy, wz;
  ImuMeasurement im{t, accgyr};
  vio_pipeline_->fillSingleImuQueue(im);
}

void Kimera::pushFrame(std::int64_t t, cv::Mat img, int cam_id) {
  if (!is_running_) return;
  if (img.channels() > 1) {
    LOG(WARNING) << "Converting img from BGR to GRAY...";
    cv::cvtColor(img, img, cv::COLOR_BGR2GRAY);
  }
  CHECK_EQ(awaiting_frame_, cam_id);
  CHECK(is_stereo_ || cam_id == 0) << "Only cam0 frames allowed on a mono pipeline";

  VioParams& vp = *vio_params_;

  bool equalize = vp.frontend_params_.stereo_matching_params_.equalize_image_;
  if (equalize) cv::equalizeHist(img, img);

  const CameraParams& cp = vp.camera_params_.at(cam_id);
  Frame::UniquePtr frame = nullptr;
  frame = make_unique<Frame>(current_k_, t, cp, img);

  if (cam_id == 0) {
    tk_.addTime(t, "frame_ts", t);
    tk_.addTime(t, "tracker_received");
  }

  if (is_stereo_) {
    if (cam_id == 0) {
      vio_pipeline_->fillLeftFrameQueue(std::move(frame));
    } else {
      StereoImuPipeline::Ptr stereo_pipeline =
          safeCast<Pipeline, StereoImuPipeline>(vio_pipeline_);
      tk_.addTime(t, "frames_received");
      stereo_pipeline->fillRightFrameQueue(std::move(frame));
      current_k_++;
    }
    awaiting_frame_ = (awaiting_frame_ + 1) % 2;
  } else {
    tk_.addTime(t, "frames_received");
    vio_pipeline_->fillLeftFrameQueue(std::move(frame));
    current_k_++;
  }

  if (!is_parallel_) vio_pipeline_->spin();
}

bool Kimera::tryDequeuePose(pose& pose) {
  VIO::Pose pose_k;
  bool dequeued = poses_.try_dequeue(pose_k);

  if (dequeued) {
    pose.px = pose_k.px;
    pose.py = pose_k.py;
    pose.pz = pose_k.pz;
    pose.rx = pose_k.rx;
    pose.ry = pose_k.ry;
    pose.rz = pose_k.rz;
    pose.rw = pose_k.rw;
    pose.timestamp = pose_k.ts;
    pose.next = nullptr;
    tk_.addTime(pose.timestamp, "monado_dequeued");
    if (tk_.in_map(pose.timestamp)) {
      pose_ext_timing_data petd = *tk_.pop(pose.timestamp);
      auto pet = make_shared<pose_ext_timing>(petd);
      pose.next = pet;
    }
  }

  return dequeued;
}

bool Kimera::tryEnqueuePose(Pose&& pose) { return poses_.try_enqueue(pose); }

void Kimera::receivePoseFromBackend(std::shared_ptr<BackendOutput>& output) {
  gtsam::Pose3 gpose = output->W_State_Blkf_.pose_;
  std::int64_t ts = output->W_State_Blkf_.timestamp_;
  tk_.addTime(ts, "pose_produced");

  Eigen::Vector3f pos = gpose.translation().cast<float>();
  Eigen::Quaternion<float, Eigen::DontAlign> rot =
      gpose.rotation().toQuaternion().cast<float>();
  tryEnqueuePose(
      Pose{ts, pos.x(), pos.y(), pos.z(), rot.x(), rot.y(), rot.z(), rot.w()});
  tk_.addTime(ts, "pose_pushed");
}

bool Kimera::supportsFeature(int feature_id) {
  return supported_features_.count(feature_id) == 1;
}

bool Kimera::useFeature(int feature_id,
                        const shared_ptr<void>& params,
                        shared_ptr<void>& result) {
  result = nullptr;
  if (feature_id == FID_EPET) {
    auto casted_params = static_pointer_cast<FPARAMS_EPET>(params);
    result = enablePoseExtTiming(*casted_params);
  } else {
    return false;
  }
  return true;
}

shared_ptr<vector<string>> Kimera::enablePoseExtTiming(bool enable) {
  tk_.timing_enabled = enable;
  return make_shared<vector<string>>(timing_titles);
}

}  // namespace VIO

namespace xrt::auxiliary::tracking::slam {

const int IMPLEMENTATION_VERSION_MAJOR = HEADER_VERSION_MAJOR;
const int IMPLEMENTATION_VERSION_MINOR = HEADER_VERSION_MINOR;
const int IMPLEMENTATION_VERSION_PATCH = HEADER_VERSION_PATCH;

struct slam_tracker::implementation {
  // Let's just point to our existing Kimera wrapper class instead of
  // reimplement it
  Kimera* kimera;
};

slam_tracker::slam_tracker(const slam_config& config) {
  CHECK(config.cam_count == 1 || config.cam_count == 2);
  impl = make_unique<slam_tracker::implementation>();
  impl->kimera = new Kimera{*config.config_file};
}

slam_tracker::~slam_tracker() { delete impl->kimera; }

void slam_tracker::initialize() {}
void slam_tracker::start() { impl->kimera->start(); }
bool slam_tracker::is_running() { return impl->kimera->isRunning(); }
void slam_tracker::stop() { impl->kimera->stop(); }
void slam_tracker::finalize() {}

void slam_tracker::push_imu_sample(const imu_sample& s) {
  impl->kimera->pushImuSample(s.timestamp, s.ax, s.ay, s.az, s.wx, s.wy, s.wz);
}

void slam_tracker::push_frame(const img_sample& sample) {
  impl->kimera->pushFrame(sample.timestamp, sample.img, sample.cam_index);
}

bool slam_tracker::try_dequeue_pose(pose& pose) {
  return impl->kimera->tryDequeuePose(pose);
}

bool slam_tracker::supports_feature(int feature_id) {
  return impl->kimera->supportsFeature(feature_id);
};

bool slam_tracker::use_feature(int feature_id,
                               const shared_ptr<void>& params,
                               shared_ptr<void>& result) {
  return impl->kimera->useFeature(feature_id, params, result);
}

}  // namespace xrt::auxiliary::tracking::slam
