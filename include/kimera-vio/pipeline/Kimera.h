// Copyright 2021, Collabora, Ltd.

/**
 * @file   Kimera.h
 * @brief  Expose Kimera as a library for external usage
 * @author Mateo de Mayo <mateo.demayo@collabora.com>
 */

#pragma once

#include <map>
#include <memory>
#include <mutex>
#include <opencv2/core/mat.hpp>
#include <unordered_set>
#include <vector>

#include "kimera-vio/third_party/monado/slam_tracker.hpp"
#include "kimera-vio/third_party/readerwriterqueue/readerwriterqueue.h"

using namespace xrt::auxiliary::tracking::slam;

namespace VIO {

class Pipeline;
class BackendOutput;
class CameraParams;
class VioParams;
class DataProviderInterface;

// Keep track of internal timestamps for an input set
struct Timekeeper {
 private:
  std::map<int64_t, std::shared_ptr<timestats>> stats{};
  std::mutex stats_lock{};

 public:
  bool timing_enabled = false;
  const std::vector<std::string>* timing_titles;

  Timekeeper(bool e, const std::vector<std::string>* tt)
      : timing_enabled(e), timing_titles(tt) {}

  void addTime(int64_t t, const char* name, int64_t ts = INT64_MIN) {
    std::lock_guard<std::mutex> _{stats_lock};
    bool in_map = stats.count(t) == 1;
    if (in_map) {
      stats[t]->addTime(name, ts);
    } else if (timing_enabled) {
      stats[t] = std::make_shared<timestats>();
      stats[t]->timing_enabled = true;
      stats[t]->timing_titles = timing_titles;
      stats[t]->addTime(name, ts);
    }
  }

  std::shared_ptr<timestats> pop(int64_t t) {
    std::lock_guard<std::mutex> _{stats_lock};
    auto stat = stats[t];
    stats.erase(t);
    return stat;
  }

  bool in_map(int64_t t) {
    std::lock_guard<std::mutex> _{stats_lock};
    return stats.count(t) == 1;
  }
};

struct Pose {
  std::int64_t ts;
  float px, py, pz;
  float rx, ry, rz, rw;
};

class Kimera {
 public:
  Kimera(std::string flagfile);
  void start();
  void stop();
  bool isRunning();

  void pushImuSample(std::int64_t t,
                     double ax,
                     double ay,
                     double az,
                     double wx,
                     double wy,
                     double wz);
  void pushFrame(std::int64_t t, cv::Mat img, int cam_id);

  bool tryDequeuePose(pose& pose);

  bool supportsFeature(int feature_id);
  bool useFeature(int feature_id,
                  const std::shared_ptr<void>& params,
                  std::shared_ptr<void>& result);

 private:
  bool tryEnqueuePose(Pose&& pose);
  void receivePoseFromBackend(std::shared_ptr<BackendOutput>& output);

  std::shared_ptr<Pipeline> vio_pipeline_;
  std::shared_ptr<VioParams> vio_params_;
  std::shared_ptr<DataProviderInterface> dataset_parser_;
  bool is_parallel_;
  bool is_stereo_;

  bool is_running_ = false;
  int awaiting_frame_ = 0;  // Forces left-right interleaved pushFrame
  std::uint64_t current_k_ = 0;

  // TODO: Determine a reasonable initial size instead of just 100
  moodycamel::ReaderWriterQueue<Pose> poses_{100};

  // slam_tracker features
  std::unordered_set<int> supported_features_{F_ENABLE_POSE_EXT_TIMING};
  Timekeeper tk_;
  std::shared_ptr<std::vector<std::string>> enablePoseExtTiming(bool enable);
};

}  // namespace VIO
